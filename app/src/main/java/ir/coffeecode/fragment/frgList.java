package ir.coffeecode.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Developer on 9/30/2016.
 */
public class frgList extends Fragment {
    View view;
    TextView txvAlert;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frg_list, container, false);

        txvAlert = (TextView) view.findViewById(R.id.txvAlert);
        Button btn = (Button) view.findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txvAlert.setText("Done!");
                Message message = new Message();
                message.setMessage("Hello everyone!");
                EventBus.getDefault().post(message);
            }
        });

        return view;
    }
}
