package ir.coffeecode.fragment;

/**
 * Created by Developer on 10/3/2016.
 */
public class Message {
    private String _message;

    public String getMessage() {
        return _message;
    }

    public void setMessage(String message) {
        _message = message;
    }
}
