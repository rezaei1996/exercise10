package ir.coffeecode.fragment;

import android.app.FragmentManager;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.Surface;

public class actMain extends AppCompatActivity {
    frgList frgList;
    frgContent frgContent;
    FragmentManager manager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_main);

        manager = getFragmentManager();
        frgList = new frgList();
        frgContent = new frgContent();

        Display display = getWindowManager().getDefaultDisplay();
        int intRotation;
        intRotation = display.getRotation();
        if (intRotation == Surface.ROTATION_0){
            manager.beginTransaction().add(R.id.lytMain,frgList).commit();
        }else if (intRotation == Surface.ROTATION_90){
            manager.beginTransaction().add(R.id.lytList,frgList).add(R.id.lytContent,frgContent).commit();
        }

    }
}
